package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
/**
 * @EnableConfigServer - 开启配置中心服务端。
 *  应用会根据全局配置文件访问GIT远程仓库，并将远程仓库中的配置内容下载到本地。
 */

@EnableConfigServer
public class Springcloud05ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springcloud05ConfigServerApplication.class, args);
    }

}
