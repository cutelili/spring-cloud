package com.bjsxt.service;

import com.bjsxt.service.impl.ClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "application-service",fallback = ClientServiceImpl.class)
public interface ClientService extends ServiceApi {
}
