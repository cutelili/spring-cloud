package com.bjsxt.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecondController {
    @GetMapping("/showInfo")
    public String showInfo(){
        return "SecondController的showInfo方法执行！";

    }
}
