package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@SpringBootApplication
/**
 * @EnableCircuitBreaker - 开启Hystrix容错处理能力。
 *  如果不使用此注解，服务代码中的@HystrixCommand注解无效。
 */

@EnableCircuitBreaker
/**
 * @EnableCaching - 开启spring cloud对cache的支持。
 *  * 可以自动的使用请求缓存，访问redis等cache服务。
 */
@EnableCaching
public class Springcloud04ConsumerHystrixApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springcloud04ConsumerHystrixApplication.class, args);
    }

}
