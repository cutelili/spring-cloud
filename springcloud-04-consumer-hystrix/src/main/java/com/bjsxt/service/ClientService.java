package com.bjsxt.service;

public interface ClientService {
    String getInfo();
    String testGet();
    String testPost();
}
