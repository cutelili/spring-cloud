package com.bjsxt.service.impl;

import ch.qos.logback.core.net.server.Client;
import com.bjsxt.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private LoadBalancerClient loadBalancerClient;
    @Override
    public String getInfo() {
        ServiceInstance si = loadBalancerClient.choose("application-service");
        //创建RestTemplate对象
        RestTemplate restTemplate = new RestTemplate();
        // 拼接访问服务的URL  http://ip:port/
        StringBuilder sb = new StringBuilder();
        sb.append("http://").append(si.getHost()).append(":").append(si.getPort());

        ResponseEntity<String> response = restTemplate.exchange(sb.toString(), HttpMethod.GET, null, String.class);
        return response.getBody();
    }
}
