package com.bjsxt.controller;

import com.bjsxt.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {
    @Autowired
    private ClientService clientService;
    @GetMapping
    public String showInfo(){
        return clientService.getInfo();
    }
}
