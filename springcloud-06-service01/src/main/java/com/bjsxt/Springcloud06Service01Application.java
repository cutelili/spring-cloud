package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springcloud06Service01Application {

    public static void main(String[] args) {
        SpringApplication.run(Springcloud06Service01Application.class, args);
    }

}
