package com.bjsxt.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
    @GetMapping
    public Object showInfo(){
        return "测试Spring Cloud Netflix Ribbon开发服务提供者01";
    }
}
