package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springcloud01Provider01Application {

    public static void main(String[] args) {
        SpringApplication.run(Springcloud01Provider01Application.class, args);
    }

}
